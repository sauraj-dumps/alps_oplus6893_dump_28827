#!/bin/bash

cat my_preload/app/Photos_app/Photos.apk.* 2>/dev/null >> my_preload/app/Photos_app/Photos.apk
rm -f my_preload/app/Photos_app/Photos.apk.* 2>/dev/null
cat my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null >> my_heytap/priv-app/Velvet/Velvet.apk
rm -f my_heytap/priv-app/Velvet/Velvet.apk.* 2>/dev/null
cat my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null >> my_heytap/priv-app/GmsCore/GmsCore.apk
rm -f my_heytap/priv-app/GmsCore/GmsCore.apk.* 2>/dev/null
cat my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null >> my_heytap/app/WebViewGoogle/WebViewGoogle.apk
rm -f my_heytap/app/WebViewGoogle/WebViewGoogle.apk.* 2>/dev/null
cat my_heytap/app/Maps/Maps.apk.* 2>/dev/null >> my_heytap/app/Maps/Maps.apk
rm -f my_heytap/app/Maps/Maps.apk.* 2>/dev/null
cat my_heytap/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null >> my_heytap/app/LatinImeGoogle/LatinImeGoogle.apk
rm -f my_heytap/app/LatinImeGoogle/LatinImeGoogle.apk.* 2>/dev/null
cat my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null >> my_heytap/app/Gmail2/Gmail2.apk
rm -f my_heytap/app/Gmail2/Gmail2.apk.* 2>/dev/null
cat my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null >> my_heytap/app/YouTube/YouTube.apk
rm -f my_heytap/app/YouTube/YouTube.apk.* 2>/dev/null
cat my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null >> my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk
rm -f my_heytap/app/TrichromeLibrary/TrichromeLibrary.apk.* 2>/dev/null
cat system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null >> system/system/system_ext/priv-app/Settings/Settings.apk
rm -f system/system/system_ext/priv-app/Settings/Settings.apk.* 2>/dev/null
cat system/system/system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null >> system/system/system_ext/priv-app/SystemUI/SystemUI.apk
rm -f system/system/system_ext/priv-app/SystemUI/SystemUI.apk.* 2>/dev/null
cat system/system/system_ext/priv-app/OPLauncher2/OPLauncher2.apk.* 2>/dev/null >> system/system/system_ext/priv-app/OPLauncher2/OPLauncher2.apk
rm -f system/system/system_ext/priv-app/OPLauncher2/OPLauncher2.apk.* 2>/dev/null
cat system/system/system_ext/app/OplusEngineerCamera/OplusEngineerCamera.apk.* 2>/dev/null >> system/system/system_ext/app/OplusEngineerCamera/OplusEngineerCamera.apk
rm -f system/system/system_ext/app/OplusEngineerCamera/OplusEngineerCamera.apk.* 2>/dev/null
cat system/system/apex/com.android.vndk.current.apex.* 2>/dev/null >> system/system/apex/com.android.vndk.current.apex
rm -f system/system/apex/com.android.vndk.current.apex.* 2>/dev/null
cat system/system/apex/com.android.art.release.apex.* 2>/dev/null >> system/system/apex/com.android.art.release.apex
rm -f system/system/apex/com.android.art.release.apex.* 2>/dev/null
cat vendor_boot/vendor_boot.img.* 2>/dev/null >> vendor_boot/vendor_boot.img
rm -f vendor_boot/vendor_boot.img.* 2>/dev/null
cat my_stock/del-app/OPBreathMode/OPBreathMode.apk.* 2>/dev/null >> my_stock/del-app/OPBreathMode/OPBreathMode.apk
rm -f my_stock/del-app/OPBreathMode/OPBreathMode.apk.* 2>/dev/null
cat my_stock/del-app/CanvasResources/CanvasResources.apk.* 2>/dev/null >> my_stock/del-app/CanvasResources/CanvasResources.apk
rm -f my_stock/del-app/CanvasResources/CanvasResources.apk.* 2>/dev/null
cat my_stock/app/OPAppCategoryProvider/OPAppCategoryProvider.apk.* 2>/dev/null >> my_stock/app/OPAppCategoryProvider/OPAppCategoryProvider.apk
rm -f my_stock/app/OPAppCategoryProvider/OPAppCategoryProvider.apk.* 2>/dev/null
cat my_stock/app/Aod/Aod.apk.* 2>/dev/null >> my_stock/app/Aod/Aod.apk
rm -f my_stock/app/Aod/Aod.apk.* 2>/dev/null
cat my_stock/app/OplusCamera/OplusCamera.apk.* 2>/dev/null >> my_stock/app/OplusCamera/OplusCamera.apk
rm -f my_stock/app/OplusCamera/OplusCamera.apk.* 2>/dev/null
cat odm/lib64/libstblur_capture_api.so.* 2>/dev/null >> odm/lib64/libstblur_capture_api.so
rm -f odm/lib64/libstblur_capture_api.so.* 2>/dev/null
cat vendor_boot.img.* 2>/dev/null >> vendor_boot.img
rm -f vendor_boot.img.* 2>/dev/null
cat recovery.img.* 2>/dev/null >> recovery.img
rm -f recovery.img.* 2>/dev/null
